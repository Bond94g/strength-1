<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <?
    use Bitrix\Main\Page\Asset;
    $APPLICATION->ShowHead();
    ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <?
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/font-awesome.min.css");

    Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');
    Asset::getInstance()->addString("<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese' rel='stylesheet'>");
    Asset::getInstance()->addString("<link href='//fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese' rel='stylesheet'>");

    ?>
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>

<!-- home -->
<div id="home">
    <!-- banner -->
    <div class="banner_w3lspvt">
        <!-- nav -->
        <div class="nav_w3ls text-center">
            <nav>
                <label for="drop" class="toggle">Menu</label>
                <input type="checkbox" id="drop"/>
                <ul class="menu">
                    <li><a href="/">Home</a></li>
                    <li><a href="#classes">Daily Classes</a></li>
                    <li>
                        <!-- First Tier Drop Down -->
                        <label for="drop-2" class="toggle toogle-2">Pages <span class="fa fa-angle-down"
                                                                                aria-hidden="true"></span>
                        </label>
                        <a href="#">Pages <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                        <input type="checkbox" id="drop-2"/>
                        <ul class="text-left">
                            <li><a href="#about" class="drop-text">About Us</a></li>
                            <li><a href="#services" class="drop-text">Services</a></li>
                            <li><a href="#news" class="drop-text">Latest News</a></li>
                            <li><a href="#classes" class="drop-text">Popular Classes</a></li>
                        </ul>
                    </li>
                    <li><a href="#gallery">Gallery</a></li>
                    <li><a href="#contact">Contact Us</a></li>
                </ul>
            </nav>
        </div>
        <!-- //nav -->
        <div class="banner-text text-center">
            <div class="banner-bot">
                <h1><a href="index.html" class="logo">Strength</a></h1>
                <p class="my-3">Nemo enim ipsam voluptatem quia voluptas sit
                    aspernatur aut odit aut fugit.
                </p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="social-icons mt-4">
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">
                                    <span class="fa fa-facebook"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-twitter"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-instagram"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-pinterest-p"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="text-right">
                        <a href="#about" class="btn button-style">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //banner -->
</div>
<!-- //home -->

<!-- main background image -->
<div class="bottom-w3layouts">