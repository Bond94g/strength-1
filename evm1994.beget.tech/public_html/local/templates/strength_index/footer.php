<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
</div>
<!-- //main background image -->

<!-- footer -->
<footer class="copyright-w3ls py-4">
    <div class="container">
        <div class="row">
            <!-- copyright -->
            <p class="col-lg-8 copy-right-grids text-lg-left text-center mt-lg-2">© 2019 Strength. All
                Rights Reserved | Design by
                <a href="https://w3layouts.com/" target="_blank" class="text-colors">W3layouts</a>
            </p>
            <!-- //copyright -->
            <!-- social icons -->
            <div class="col-lg-4 w3social-icons text-lg-right text-center mt-lg-0 mt-3">
                <ul>
                    <li>
                        <a href="#" class="rounded-circle">
                            <span class="fa fa-facebook-f"></span>
                        </a>
                    </li>
                    <li class="px-2">
                        <a href="#" class="rounded-circle">
                            <span class="fa fa-google-plus"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="rounded-circle">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </li>
                    <li class="pl-2">
                        <a href="#" class="rounded-circle">
                            <span class="fa fa-dribbble"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- //social icons -->
        </div>
    </div>
</footer>
<!-- //footer -->
<!-- move top icon -->
<a href="#home" class="move-top text-center">
    <span class="fa fa-hand-o-up" aria-hidden="true"></span>
</a>
<!-- //move top icon -->

</body>

</html>